



-- -----------------------------------------------------------
-- Entity Designer DDL Script for MySQL Server 4.1 and higher
-- -----------------------------------------------------------
-- Date Created: 12/17/2013 11:13:10
-- Generated from EDMX file: C:\Users\Administrator\Documents\TReactor-master\XServer\XData\xModel.edmx
-- Target version: 2.0.0.0
-- --------------------------------------------------


-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- NOTE: if the constraint does not exist, an ignorable error will be reported.
-- --------------------------------------------------

--    ALTER TABLE `BansSet` DROP CONSTRAINT `FK_MembersBans`;

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------
SET foreign_key_checks = 0;
    DROP TABLE IF EXISTS `MembersSet`;
    DROP TABLE IF EXISTS `BansSet`;
SET foreign_key_checks = 1;

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

CREATE TABLE `MembersSet`(
	`Id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`Username` varchar (1000) NOT NULL, 
	`Password` varchar (1000) NOT NULL, 
	`Email` varchar (1000) NOT NULL);

ALTER TABLE `MembersSet` ADD PRIMARY KEY (Id);




CREATE TABLE `BansSet`(
	`Id` int NOT NULL AUTO_INCREMENT UNIQUE, 
	`MembersId` int NOT NULL, 
	`Type` smallint NOT NULL, 
	`Time` datetime NOT NULL, 
	`Banner` varchar (1000) NOT NULL);

ALTER TABLE `BansSet` ADD PRIMARY KEY (Id);






-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on `MembersId` in table 'BansSet'

ALTER TABLE `BansSet`
ADD CONSTRAINT `FK_MembersBans`
    FOREIGN KEY (`MembersId`)
    REFERENCES `MembersSet`
        (`Id`)
    ON DELETE NO ACTION ON UPDATE NO ACTION;

-- Creating non-clustered index for FOREIGN KEY 'FK_MembersBans'

CREATE INDEX `IX_FK_MembersBans` 
    ON `BansSet`
    (`MembersId`);

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------
